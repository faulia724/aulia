 
  </div>

<!-- jQuery -->
<script src="static/js/jquery-3.3.1.slim.min.js"></script>

<!-- Plugins -->
<script src="static/plugin/bootstrap/js/popper.min.js"></script>
<script src="static/plugin/bootstrap/js/bootstrap.min.js"></script>
<script src="static/plugin/owl-carousel/js/owl.carousel.min.js"></script>
<script src="static/plugin/typeit-master/typeit.min.js"></script>
<script src="static/plugin/isotope/isotope.pkgd.min.js"></script>
<script src="static/plugin/magnific/jquery.magnific-popup.min.js"></script>

<script src="static/plugin/nav/js/classie.js"></script>
<script src="static/plugin/nav/js/main.js"></script>



<!-- custom -->
<script src="static/js/custom.js"></script>

</body>
<!-- Body End -->


<!-- Mirrored from inventheme.com/themeforest/alexio/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 13 Apr 2019 12:09:24 GMT -->
</html>