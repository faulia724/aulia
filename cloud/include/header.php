<!DOCTYPE html>
<html lang="zxx" class="no-js">


<!-- Mirrored from inventheme.com/themeforest/alexio/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 13 Apr 2019 12:07:37 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!--
  ========================================================================
  EXCLUSIVE ON themeforest.net
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  Template Name   : Alexio
  Author          : PxDraft
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  Copyright (c) 2018 - PxDraft
  ========================================================================
  -->
  <!-- Page Title -->
  <title>Alexio - Personal Portfolio</title>
  <!-- / -->

  <!---Font Icon-->
  <link href="static/plugin/font-awesome/css/fontawesome-all.min.css" rel="stylesheet">
  <link href="static/plugin/themify-icons/themify-icons.css" rel="stylesheet">
  <!-- / -->

  <!-- Plugin CSS -->
  <link href="static/plugin/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="static/plugin/owl-carousel/css/owl.carousel.min.css" rel="stylesheet">
  <link href="static/plugin/magnific/magnific-popup.css" rel="stylesheet">
  <link href="static/plugin/nav/css/component.css" rel="stylesheet" />
  <!-- / -->

  <!-- Theme Style -->

  <link href="static/css/styles.css" rel="stylesheet">
  <link href="static/css/color/default.css" rel="stylesheet">

  <script src="static/plugin/nav/js/modernizr-custom.js"></script>
  <!-- / -->

  <!-- Favicon -->
  <link rel="icon" href="favicon.ico" />
  <!-- / -->


</head>

<!-- Body Start -->
<body class="dark-body">
   <!-- Loading -->
  <div id="loading">
    <div class="load-circle"><span class="one"></span></div>
  </div>
  <!-- / -->


  <div class="pages-stack">