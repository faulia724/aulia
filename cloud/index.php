<?php include( 'include/header.php' ) ?>

<!-- 
====================
Home Banner
====================
    -->
    <div class="page home-banner white-bg" id="home">
    <div class="container-fluid p-0">
    <div class="row no-gutters full-screen">
        <div class="col-lg-3 col-xl-4 blue-bg">
        <div class="d-flex align-items-end home-user-avtar v-center-box">
            <img src="static/img/user.jpg" title="" alt="">
        </div>
        </div>
        <div class="col-lg-9 col-xl-8">
        <div class="page-scroll">
            <div class="page-content">
            <div class="v-center-box d-flex align-items-center">
                <div class="home-text">
                <h6 class="dark-color theme-after"><?= 'Selamat Datang' ?></h6>
                <h1 class="dark-color blue-after"><?= 'SAYA, FITRI AULIA'; ?></h1>
                <div class="row">
                    <div class="col-sm-6">
                        <p><?= 'Jurusan'  ?></p>
                    </div>
                    <div class="col-sm-6">
                        <p><?= 'Teknik Informatika'  ?></p>
                    </div>
                </div>
                </div>
                <ul class="social-icons">
                <li><a class="facebook" href="#"><i class="fab fa-facebook-f"></i></a></li>
                <li><a class="twitter" href="#"><i class="fab fa-twitter"></i></a></li>
                <li><a class="google" href="#"><i class="fab fa-google-plus-g"></i></a></li>
                <li><a class="linkedin" href="#"><i class="fab fa-linkedin-in"></i></a></li>
                </ul>
            </div>
            </div> 
            </div>
        </div>
    </div>
    </div>
</div>


<?php include( 'include/footer.php' ) ?>